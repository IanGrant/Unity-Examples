public enum SceneIndex
{
    MANAGER = 0,
    TITLE_SCREEN = 1,
    MAP = 2,
    NAVMESH = 3,
}
