using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    public GameObject loadingScreen;
    public ProgressBar bar;
    public TextMeshProUGUI textField;
    public Sprite[] backgrounds;
    public Image backgroundImage;
    public TextMeshProUGUI tipsText;
    public CanvasGroup alphaCanvas;
    public string[] tips;

    private void Awake()
    {
        instance = this;

        SceneManager.LoadSceneAsync((int)SceneIndex.TITLE_SCREEN, LoadSceneMode.Additive);
    }

    List<AsyncOperation> sceneLoading = new List<AsyncOperation>();
    public void LoadGame()
    {
        backgroundImage.sprite = backgrounds[Random.Range(0, backgrounds.Length)];
        loadingScreen.gameObject.SetActive(true);

        StartCoroutine(GenerateTips());

        sceneLoading.Add(SceneManager.UnloadSceneAsync((int)SceneIndex.TITLE_SCREEN));
        sceneLoading.Add(SceneManager.LoadSceneAsync((int)SceneIndex.MAP, LoadSceneMode.Additive));
        sceneLoading.Add(SceneManager.LoadSceneAsync((int)SceneIndex.NAVMESH, LoadSceneMode.Additive));

        StartCoroutine(GetSceneLoadProgress());
        StartCoroutine(GetTotalProgress());
    }

    float totalSceneProgress;
    float totalSpawnProgress;

    public IEnumerator GetSceneLoadProgress()
    {
        for(int i=0; i <sceneLoading.Count; i++)
        {
            while (!sceneLoading[i].isDone)
            {

                totalSceneProgress = 0;

                foreach(AsyncOperation operation in sceneLoading)
                {
                    totalSceneProgress += operation.progress;
                }

                totalSceneProgress = (totalSceneProgress / sceneLoading.Count) * 100f;
                textField.text = string.Format("Loading: {0}%", totalSceneProgress);

                yield return null;
            }
        }


    }

    public IEnumerator GetTotalProgress()
    {
        float totalProgress = 0;

        while (Initialisation.current == null || !Initialisation.current.isDone)
        {
            if (Initialisation.current == null)
            {
                totalSpawnProgress = 0;
            }
            else
            {
                totalSpawnProgress = Mathf.Round(Initialisation.current.progress * 100f);

                switch (Initialisation.current.currentStage)
                {
                    case Initialisation.Pedestrians:
                        textField.text = string.Format("Loading Pedestrians: {0}%", totalSceneProgress);
                        break;
                    case Initialisation.Traffic:
                        textField.text = string.Format("Loading Traffic:  {0}%", totalSceneProgress);
                    default:
                        textField.text = string.Format("Loading: {0}%", totalSceneProgress);
                        break;
                }

            }

            totalProgress = Mathf.Round((totalSceneProgress + totalSpawnProgress) / 2f);

            bar.current = Mathf.RoundToInt(totalProgress);
        }


        loadingScreen.gameObject.SetActive(false);
        bar.current = Mathf.RoundToInt(totalSceneProgress);
    }

    public int tipCount;
    public IEnumerator GenerateTips()
    {
        tipCount = Random.Range(0, tips.Length);
        tipsText.text = tips[tipCount];
        while (loadingScreen.activeInHierarchy)
        {
            yield return new WaitForSeconds(3f);

           
            LeanTween.alphaCanvas(alphaCanvas, 0, 0.5f);

            yield return new WaitForSeconds(0.5f);

            tipCount++;
            if(tipCount >= tips.Length)
            {
                tipCount = 0;
            }

            tipsText.text = tips[tipCount];

            LeanTween.alphaCanvas(alphaCanvas, 1, 0.5f);



        }
    }

}
